package com.gxgeek.weixin.quartzs;

import com.alibaba.fastjson.JSON;
import com.gxgeek.weixin.common.util.HttpClientUtil;
import com.gxgeek.weixin.domain.dto.AccessToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.gxgeek.weixin.common.util.WeixinUtil.ACCESS_TOKEN_URL;
import static com.gxgeek.weixin.common.util.WeixinUtil.APPID;
import static com.gxgeek.weixin.common.util.WeixinUtil.APPSECRET;
import static com.gxgeek.weixin.domain.enums.Constant.ACCESSTOKEN;
import static com.gxgeek.weixin.domain.enums.Constant.setAccessToken;

/**
 * Created by gx on 2017/4/19.
 */
//执行定时任务获取 AccessToken
@Component
@Slf4j
public class AccessTokenScheduled {

    /**
     * 每两个小时执行一次 更新AccessToken
     */
    @Scheduled(cron = "0 0 0/2 * * ?")
    public void reportCurrentTime() {
        String url = ACCESS_TOKEN_URL.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
        String json = HttpClientUtil.getInstance().sendHttpsGet(url);
        AccessToken accessToken = JSON.parseObject(json,AccessToken.class);
        log.info(accessToken.getAccess_token());
        setAccessToken(accessToken.getAccess_token());
        log.info(ACCESSTOKEN);
    }

}
