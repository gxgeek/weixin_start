package com.gxgeek.weixin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Created by gx on 2017/3/27.
 */
@Configuration
public class BaseConfig {
    //线程池
    @Bean(name = "executor")
    public ThreadPoolTaskExecutor cosumerTaskExecutor(@Value("${consumer.corePoolSize}") int corePoolSize, @Value("${consumer.maxPoolSize}") int maxPoolSize) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        return executor;
    }





}
