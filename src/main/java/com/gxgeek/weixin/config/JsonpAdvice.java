//package com.gegeek.weixin.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;
//
///**
// * Created by gx on 2017/4/10.
// */
//@Slf4j
//@ControllerAdvice
//public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
//    public JsonpAdvice() {
//        super("callback", "jsonp");
//    }
//
//
//}
