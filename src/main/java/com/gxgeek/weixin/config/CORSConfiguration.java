//package com.gegeek.weixin.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//
///**
// * Created by gx on 2017/4/12.
// */
////CORS 全局设置
//@Configuration
//public class CORSConfiguration {
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**")
//                        .allowedHeaders("*")
//                        .allowedMethods("*")
//                        .allowedOrigins("*");
//            }
//        };
//    }
////    @Bean
////    public CorsFilter corsFilter() {
////        final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
////        final CorsConfiguration corsConfiguration = new CorsConfiguration();
////        corsConfiguration.setAllowCredentials(true);
////        corsConfiguration.addAllowedOrigin("*");
////        corsConfiguration.addAllowedHeader("*");
////        corsConfiguration.addAllowedMethod("*");
////        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
////        return new CorsFilter(urlBasedCorsConfigurationSource);
////    }
//
//
//}
