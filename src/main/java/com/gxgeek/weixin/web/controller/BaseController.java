package com.gxgeek.weixin.web.controller;

import com.gxgeek.weixin.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.gxgeek.weixin.common.util.MessageUtil.*;
import static com.gxgeek.weixin.domain.enums.MsgType.*;

/**
 * Created by gx on 2017/4/18.
 */
@Slf4j
@RestController
public class BaseController {

    private static final String token = "gxgeek123";
    private static final String EncodingAESKey = "odfNu4BluYJA1Hgx6l8wIvSrm0bvyEFYtRVpKIxpizH";

    @Autowired
    private BaseService baseService;
    //验证微信公众号代码
    @GetMapping("/")
    public String base(@RequestParam(value = "signature",required = false)String signature,
                       @RequestParam(value = "timestamp",required = false)String timestamp,
                       @RequestParam(value = "nonce",required = false)String nonce,
                       @RequestParam(value = "echostr",required = false)String echostr){
        String [] arr = new String[]{timestamp, token, nonce};
        log.info("进入连接");
        Arrays.sort(arr);
        StringBuilder sb= new StringBuilder();
        for (String anArr : arr) {
            sb.append(anArr);
        }
        String sha1 = DigestUtils.sha1Hex(sb.toString());
        if (sha1.equals(signature)){
            return echostr;
        }
        return "";
    }

    @PostMapping("/")
    public String test(@RequestBody String xml){
        log.info("进入post连接");
        try {
            Map<String,String> map = xmlToMap(xml);
            String MsgType = map.get("MsgType");
            String FromUserName = map.get("FromUserName");
            String ToUserName = map.get("ToUserName");
            String CreateTime = map.get("CreateTime");
            String content = map.get("Content");
            String message ="初始化";


            //当接受到文本信息时应该做的
            if (MsgType.equals(TEXT.getValue())){
                log.info("收到消息"+content);
                return baseService.textMsg(ToUserName,FromUserName,content);
            } else if (MsgType.equals(IMAGE.getValue())){//接收图文推送
                return baseService.imageMsg(ToUserName,FromUserName,content);
            } else if (MsgType.equals(EVENT.getValue())){//接收事件推送
                String event = map.get("Event");
                return baseService.eventMsg(map,event,ToUserName,FromUserName,content);
            }else if (MsgType.equals(LOCATION.getValue())){//接收地理位置事件推送
                String event = map.get("Event");
                return baseService.locationMsg(map,event,ToUserName,FromUserName,content);
            }
        } catch (DocumentException e) {
            log.error(e.getMessage(),e);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return "";
    }


    @GetMapping("/test")
    public String test(){
        return "可以正常访问";
    }


//    private String
}
