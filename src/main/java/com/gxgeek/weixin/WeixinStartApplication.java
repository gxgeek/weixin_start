package com.gxgeek.weixin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.gxgeek.weixin.domain.dao*") // mybatis 中的注解 扫描 mybatis中的 dao接口
@SpringBootApplication//注解等价于以默认属性使用 @Configuration ， @EnableAutoConfiguration 和 @ComponentScan 。
@EnableScheduling// 这个注解使@Scheduled注解生效产生定时任务
@ServletComponentScan//扫描@WebFilter注解自动注册 使过滤器产生作用，这种方式需要在 主配置类 上加
public class WeixinStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeixinStartApplication.class, args);
	}
}
