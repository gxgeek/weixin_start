package com.gxgeek.weixin.domain.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author gx
 * @since 2017-04-19
 */
public class Content implements Serializable {

    private static final long serialVersionUID = 1L;

	private String content;
    /**
     * 1. 关键字回复 2.关注回复
     */
	private Integer type;
	private String keyword;
	@TableId(type = IdType.AUTO)
	private Integer id;


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
