package com.gxgeek.weixin.domain.enums;

/**
 * Created by gx on 2017/4/19.
 */
public class Constant {
    //请求图灵接口地址
    public static final String TURING_URL="http://www.tuling123.com/openapi/api";
    //创建菜单接口地址
    public static final String WEIXIN_CREATMENU_URL="https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
    //查询菜单地址
    public static final String WEIXIN_QUERYMENU_URL="https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";
    public static final String WEIXIN_DELETE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";

    public static final String TURING_KEY="39b515f864cf4f4bae277a042533f4d8";



    //点击推事件用户点击click类型按钮后，微信服务器会通过消息接口推送消息类型为event的结构给开发者（参考消息接口指南），并且带上按钮中开发者填写的key值，开发者可以通过自定义的key值与用户进行交互
    public static final String BUTTON_TYPE_CLICK="click";
    //view：跳转URL用户点击view类型按钮后，微信客户端将会打开开发者在按钮中填写的网页URL，可与网页授权获取用户基本信息接口结合，获得用户基本信息。
    public static final String BUTTON_TYPE_VIEW="view";
    //scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。
    public static final String BUTTON_TYPE_SCANCODE_WAITMSG= "scancode_waitmsg";
    //scancode_push：扫码推事件用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。
    public static final String BUTTON_TYPE_SCANCODE_PUSH= "scancode_push";
    //location_select：弹出地理位置选择器用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息。
    public static final String BUTTON_TYPE_LOCATION_SELECT= "location_select";
    //跳转图文消息URL用户点击view_limited类型按钮后，微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL，永久素材类型只支持图文消息。请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。
    public static final String BUTTON_TYPE_SCANCODE_VIEW_LIMITED="39b515f864cf4f4bae277a042533f4d8";




    //调用微信接口凭证
    public static  String ACCESSTOKEN;

    public static void setAccessToken(String accessToken) {
        Constant.ACCESSTOKEN = accessToken;
    }
}
