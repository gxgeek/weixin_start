package com.gxgeek.weixin.domain.enums;

/**
 * Created by gx on 2017/4/18.
 */
public enum  MsgType {
    //文本消息
    TEXT("text"),
    //图片消息
    IMAGE("image"),
    //图文消息
    NEWS("news"),
    //图文消息
    MUSIC("music"),
    //声音
    VOICE("voice"),
    //    视频
    VIDEO("video"),
    //小视频
    SHORTVIDEO("shortvideo"),
    //链接
    LINK("link"),
    //地址
    LOCATION("location"),

    EVENT("event"),
    SUBSCRIBE("subscribe"),
    UNSUBSCRIBE("unsubscribe"),
    VIEW("VIEW"),
    CLICK("CLICK");
    private String value;
    MsgType(String s) {
        this.value = s;
    }

    public String getValue() {
        return value;
    }
}