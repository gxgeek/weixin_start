//package com.gxgeek.weixin.domain.po;
//
//import lombok.Data;
//
//import java.util.List;
//
///**
// * Created by gx on 2017/4/20.
// */
//@Data
//public class Menu {
//    private List<ButtonBean> button;
//
//
//    @Data
//    public static class ButtonBean {
//        /**
//         * type : click
//         * name : 今日歌曲
//         * key : V1001_TODAY_MUSIC
//         * sub_button : [{"type":"view","name":"搜索","url":"http://www.soso.com/"},{"type":"miniprogram","name":"wxa","url":"http://mp.weixin.qq.com","appid":"wx286b93c14bbf93aa","pagepath":"pages/lunar/index.html"},{"type":"click","name":"赞一下我们","key":"V1001_GOOD"}]
//         */
//        private String type;
//        private String name;
//        private String key;
//        private List<SubButtonBean> sub_button;
//
//        @Data
//        public static class SubButtonBean {
//            /**
//             * type : view
//             * name : 搜索
//             * url : http://www.soso.com/
//             * appid : wx286b93c14bbf93aa
//             * pagepath : pages/lunar/index.html
//             * key : V1001_GOOD
//             */
//
//            private String type;
//            private String name;
//            private String url;
//            private String appid;
//            private String pagepath;
//            private String key;
//
//        }
//    }
//}
