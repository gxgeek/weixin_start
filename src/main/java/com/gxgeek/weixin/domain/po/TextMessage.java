package com.gxgeek.weixin.domain.po;

import lombok.Data;

/**
 * Created by gx on 2017/4/18.
 */
@Data
public class TextMessage extends Message {
    private String Content;
    private String MsgId;


}
