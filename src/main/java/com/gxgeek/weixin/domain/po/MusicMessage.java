package com.gxgeek.weixin.domain.po;

import lombok.Data;

/**
 * Created by gx on 2017/4/19.
 */
@Data
public class MusicMessage extends Message{
    private Music Music;
}
