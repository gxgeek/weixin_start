package com.gxgeek.weixin.domain.po;

/**
 * Created by gx on 2017/4/19.
 */
public class Message {
    private String ToUserName;
    private String FromUserName;
    private Long CreateTime;
    private String MsgType;
    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public Long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Long createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(com.gxgeek.weixin.domain.enums.MsgType msgType) {
        MsgType = msgType.getValue();
    }

}
