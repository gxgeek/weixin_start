package com.gxgeek.weixin.domain.po;

import lombok.Data;

import java.util.List;

/**
 * Created by gx on 2017/4/19.
 */
@Data
public class NewMessage extends Message {
    private int ArticleCount;
    private List<News> Articles;
}
