package com.gxgeek.weixin.domain.po;

import lombok.Data;

/**
 * Created by gx on 2017/4/19.
 */
@Data
public class Music {
    private  String Title;
    private  String Description;
    private  String MusicUrl;
    private  String HQMusicUrl;
    private  String ThumbMediaId;
}
