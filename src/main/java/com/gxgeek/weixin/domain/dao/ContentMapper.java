package com.gxgeek.weixin.domain.dao;

import com.gxgeek.weixin.domain.model.Content;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author gx
 * @since 2017-04-19
 */
public interface ContentMapper extends BaseMapper<Content> {


    String selectKeyWordByType(@Param("type") int i, @Param("content") String content);

    List<String>  selectCountByType(@Param("type")int i);
}