package com.gxgeek.weixin.domain.dto;

import lombok.Data;

/**
 * Created by gx on 2017/4/20.
 */
@Data
public class ViewButton extends Button {
    private String url;
}
