package com.gxgeek.weixin.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by gx on 2017/4/20.
 */
@Data
public class Button {
    private String type;
    private String name;
    private List<Button> sub_button;
}
