package com.gxgeek.weixin.domain.dto;

import lombok.Data;

/**
 * Created by gx on 2017/4/20.
 */
@Data
public class ClickButton  extends Button{
    private String key;
}
