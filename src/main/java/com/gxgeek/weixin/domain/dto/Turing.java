package com.gxgeek.weixin.domain.dto;

import lombok.Data;

/**
 * Created by gx on 2017/4/19.
 */
@Data
public class Turing {
    private Integer code;
    private String text;

}
