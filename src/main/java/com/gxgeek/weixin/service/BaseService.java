package com.gxgeek.weixin.service;

import com.alibaba.fastjson.JSON;
import com.gxgeek.weixin.common.util.HttpClientUtil;
import com.gxgeek.weixin.domain.dao.ContentMapper;
import com.gxgeek.weixin.domain.dto.Turing;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.gxgeek.weixin.common.util.MessageUtil.*;
import static com.gxgeek.weixin.domain.enums.Constant.*;
import static com.gxgeek.weixin.domain.enums.MsgType.*;

/**
 * Created by gx on 2017/4/18.
 */
@Service
@Slf4j
public class BaseService {

    @Autowired
    private ContentMapper contentMapper;


    /**
     * 回复文本形式的  可以回复 音乐 文本 图文   当前设置了图灵机器人
     * @param toUserName
     * @param fromUserName
     * @param content
     * @return
     */
    public String textMsg(String toUserName, String fromUserName, String content) {
        log.info("接收消息~~~"+content);
        if (content.equals("1")){//收到1   回复文章 图文消息
            log.info(initNewMessage(toUserName,fromUserName));
            return initNewMessage(toUserName,fromUserName);
        }
        if (content.equals("3")){//收到3  回复图片
            String media_id = "hjGRLXIrh8DvRxWYmP80VAceRdgjcZmrS198SMFWcyKqSboRcoqUHG-z28-XmMiF";
            String message = initImageMeg(IMAGE,toUserName,fromUserName,media_id);;
            log.info("发送 消息"+message);
            return message;
        }
        if (content.equals("4")){
            String media_id = "hjGRLXIrh8DvRxWYmP80VAceRdgjcZmrS198SMFWcyKqSboRcoqUHG-z28-XmMiF";
            String message = initMusicMeg(MUSIC,toUserName,fromUserName,media_id);;
            log.info("发送 消息"+message);
            return message;

        }
        String revecontent = contentMapper.selectKeyWordByType(1,content.replace(" ","").toLowerCase());
        if (revecontent==null){
            String json = HttpClientUtil.getInstance().
                            sendHttpPost(TURING_URL,
                            new HashMap<String,String>(){{put("key",TURING_KEY);
                                put("info",content);
                                put("userid",fromUserName);
                            }});
            Turing turing = JSON.parseObject(json,Turing.class);
            revecontent = turing.getText();
            if (turing.getCode().intValue() !=100000){
                revecontent = "我听不懂你在说啥";
            }
        }
        String message = initTextMeg(TEXT,toUserName,fromUserName,revecontent);;
        log.info("发送消息消息"+message);
        return message;

    }


    /**
     * 事件触发 todo 现在只有关注回复
     *
     * @param map
     * @param event
     * @param toUserName
     * @param fromUserName
     * @param content
     * @return
     */
    public String eventMsg(Map<String, String> map, String event, String toUserName, String fromUserName, String content) {
        String revecontent;
        String message = "初始化";
        if (event.equals(SUBSCRIBE.getValue())){//关注回复
            revecontent =  contentMapper.selectCountByType(2).get(0);
            if (revecontent==null){
                revecontent = "听不懂你说啥";
            }
            message = initTextMeg(TEXT,toUserName,fromUserName,revecontent);;
        }else if (event.equals(CLICK.getValue())){//点击事件回复
            message = initNewMessage(toUserName,fromUserName);

        }else if (event.equals(VIEW.getValue())){//VIEW 事件回复
            String url = map.get("EventKey");
            message = initTextMeg(toUserName, fromUserName, url);
        }else if (event.equals(BUTTON_TYPE_SCANCODE_PUSH)){//扫码 事件回复
            String key = map.get("EventKey");
            message = initTextMeg(toUserName, fromUserName, key);
        }
        log.info("发送 消息"+message);
        return message;
    }

    /**
     * //地理位置 事件回复
     * @param map
     * @param event
     * @param toUserName
     * @param fromUserName
     * @param content
     * @return
     */
    public String locationMsg(Map<String, String> map, String event, String toUserName, String fromUserName, String content) {
            String key = map.get("Label");
            String message = initTextMeg(TEXT,toUserName, fromUserName, key);
            log.info(message);
            return message;
    }

    /**
     * 收到图片消息操作
     * @param toUserName
     * @param fromUserName
     * @param content
     * @return
     */
    public String imageMsg(String toUserName, String fromUserName, String content) {
        //后期 media_id 从数据库取出来
        String media_id = "e9FKr1btfiF3wVBm2dticCVCACpJFPqWbj1vPM0nVDG6hHWbFTBcYh3zyD52CJwl";
        String message = initImageMeg(IMAGE,toUserName,fromUserName,media_id);;
        log.info("发送 消息"+message);
        return message;
    }

}
