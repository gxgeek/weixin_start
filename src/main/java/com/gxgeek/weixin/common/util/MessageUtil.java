package com.gxgeek.weixin.common.util;

import com.gxgeek.weixin.domain.enums.MsgType;
import com.gxgeek.weixin.domain.po.*;
import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;


import java.util.*;

import static com.gxgeek.weixin.domain.enums.MsgType.NEWS;
import static org.dom4j.DocumentHelper.parseText;

/**
 * Created by gx on 2017/4/18.
 */
public class MessageUtil {

    /**
     * xml转为Map
     * @param xml
     * @return
     * @throws DocumentException
     */
    public static Map<String,String> xmlToMap(String xml) throws DocumentException {
        Map<String,String>sortedMap = new TreeMap<>();
        Document document = parseText(xml);
        Element root = document.getRootElement();
        List<Element> list =root.elements();
        list.forEach(element -> sortedMap.put(element.getName(),element.getText()));
        return sortedMap;
    }


    /**
     * 对象转XML
     * @param t
     * @param <T>
     * @return
     */
    public static <T> String objToXML(T t){
        XStream xStream = new XStream();
        //根节点变成XML
        xStream.alias("xml",t.getClass());
        return xStream.toXML(t);
    }
    /**
     * 对象转XML 图片消息
     * @param t
     * @param <T>
     * @return
     */
    public static <T> String imageMessageToXML(T t){
        XStream xStream = new XStream();
        //根节点变成XML
        xStream.alias("xml",t.getClass());
        xStream.alias("Image", Image.class);
        return xStream.toXML(t);
    }
    /**
     * 对象转XML(图文消息)
     * @param t
     * @param <T>
     * @return
     */
    public static String newMessageToXML(NewMessage t){
        XStream xStream = new XStream();
        //根节点变成XML
        xStream.alias("xml",t.getClass());
        xStream.alias("item", News.class);
        return xStream.toXML(t);
    }


    /**
     * 初始化图文消息
     * @param ToUserName
     * @param FromUserName
     * @return
     */
    public static String initNewMessage(String ToUserName, String FromUserName){
        List<News> newsList = new ArrayList<>();
        News news = new News();
        news.setDescription("~~学会IT技术哪家强");
        news.setTitle("慕课网");
        news.setPicUrl("http://imgsrc.baidu.com/forum/w%3D580/sign=23a131a1afc379317d688621dbc5b784/1dd22287e950352a9d0dd6af5343fbf2b3118bce.jpg");
        news.setUrl("http://www.imooc.com");
        newsList.add(news);


        NewMessage newMessage = new NewMessage();
        newMessage.setFromUserName(ToUserName);
        newMessage.setToUserName(FromUserName);
        newMessage.setArticles(newsList);
        newMessage.setCreateTime(new Date().getTime());
        newMessage.setMsgType(NEWS);
        newMessage.setArticleCount(newsList.size());
        return newMessageToXML(newMessage);
    }


    /**
     * 初始化
     * @param msgType
     * @param ToUserName
     * @param FromUserName
     * @param content
     * @return
     */
    public static String  initTextMeg(MsgType msgType, String ToUserName, String FromUserName, String content){
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(ToUserName);
        textMessage.setToUserName(FromUserName);
        textMessage.setMsgType(msgType);
        textMessage.setCreateTime(new Date().getTime());
        textMessage.setContent(content);
        return objToXML(textMessage);
    }
    public static String  initTextMeg(String ToUserName, String FromUserName, String content){
        TextMessage textMessage = new TextMessage();
        textMessage.setFromUserName(ToUserName);
        textMessage.setToUserName(FromUserName);
        textMessage.setCreateTime(new Date().getTime());
        textMessage.setContent(content);
        return objToXML(textMessage);
    }

    /**
     * 初始化图片消息返回
     * @param msgType
     * @param ToUserName
     * @param FromUserName
     * @param media_id
     * @return
     */
    public static String initImageMeg(MsgType msgType, String ToUserName, String FromUserName, String media_id){
        ImageMessage textMessage = new ImageMessage();
        textMessage.setFromUserName(ToUserName);
        textMessage.setToUserName(FromUserName);
        textMessage.setMsgType(msgType);
        textMessage.setCreateTime(new Date().getTime());
        textMessage.setImage(new Image(media_id));
        return objToXML(textMessage);
    }

    //todo  返回标题乱码问题
    public static String initMusicMeg(MsgType msgType, String ToUserName, String FromUserName, String media_id){
        Music music = new Music();
        music.setDescription("简单的描述");
        music.setMusicUrl("http://sgjiaf.natappfree.cc/static/See You Again.mp3");
        music.setHQMusicUrl("http://sgjiaf.natappfree.cc/static/See You Again.mp3");
        music.setThumbMediaId(media_id);
        music.setTitle("好听的");


        MusicMessage musicMsg = new MusicMessage();
        musicMsg.setMusic(music);
        musicMsg.setFromUserName(ToUserName);
        musicMsg.setToUserName(FromUserName);
        musicMsg.setMsgType(msgType);
        musicMsg.setCreateTime(new Date().getTime());
//        textMessage.setImage
        return objToXML(musicMsg);

    }


}
