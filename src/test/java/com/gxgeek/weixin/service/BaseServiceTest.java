package com.gxgeek.weixin.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.gxgeek.weixin.common.util.HttpClientUtil;
import com.gxgeek.weixin.domain.dao.ContentMapper;
import com.gxgeek.weixin.domain.model.Content;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gxgeek.weixin.common.util.MessageUtil.initTextMeg;

/**
 * Created by gx on 2017/4/19.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BaseServiceTest {

    @Autowired
    private ContentMapper contentMapper;
    @Test
    public void textMsg() throws Exception {
        String content = "Hello";

        String revecontent = contentMapper.selectKeyWordByType(1,content.replace(" ","").toLowerCase());

        System.out.println(revecontent);
        if (revecontent==null){
            revecontent = "听不懂你说啥";
        }
        log.info("发送消息~~~~~"+revecontent);
    }
    @Test
    public void eventMsg() throws Exception {
        String revecontent = contentMapper.selectCountByType(2).get(0);
        if (revecontent==null){
            revecontent = "听不懂你说啥";
        }
//        String message = initTextMeg(TEXT,toUserName,fromUserName,revecontent);;
        log.info("收到消息"+revecontent);

    }
    @Test
    public void eventMsg1() throws Exception {
        String content = "你";
        Map map = new HashMap();
        map.put("type",1);
        map. put("keyword",content);
        List x = contentMapper.selectByMap(map);
        List y = contentMapper.selectList(new EntityWrapper<Content>().where("type = {0} and  keyword = {1} ",1,content));

        System.out.println(JSON.toJSON(x));
        System.out.println(JSON.toJSON(y));
    }
    @Test
    public void eventMsg2() throws Exception {
        String a = HttpClientUtil.getInstance().
                sendHttpPost("http://www.tuling123.com/openapi/api",
                        new HashMap(){{put("key","39b515f864cf4f4bae277a042533f4d8");
                                       put("info","你叫什么");
                                       put("userid","123");
                                                    }});
        String content = "你";
        Map map = new HashMap();
        map.put("type",1);
        map. put("keyword",content);
        List x = contentMapper.selectByMap(map);
        List y = contentMapper.selectList(new EntityWrapper<Content>().where("type = {0} and  keyword = {1} ",1,content));

        System.out.println(JSON.toJSON(x));
        System.out.println(JSON.toJSON(y));
    }


}