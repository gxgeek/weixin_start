package com.gxgeek.weixin.common.util;

import com.alibaba.fastjson.JSON;
import com.gxgeek.weixin.domain.dto.AccessToken;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static com.gxgeek.weixin.common.util.WeixinUtil.APPID;
import static com.gxgeek.weixin.common.util.WeixinUtil.APPSECRET;
import static com.gxgeek.weixin.common.util.WeixinUtil.initMenu;

/**
 * Created by gx on 2017/4/19.
 */
@Slf4j
public class WeixinUtilTest {
    /**
     * 删除菜单
     * @throws Exception
     */
    @Test
    public void delMenu() throws Exception {
        String url = ACCESS_TOKEN_URL.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
        String json = HttpClientUtil.getInstance().sendHttpsGet(url);
        AccessToken accessToken = JSON.parseObject(json,AccessToken.class);
        System.out.println("票据"+accessToken.getAccess_token());
        System.out.println("有效时间"+accessToken.getExpires_in());
        String json1 = WeixinUtil.delMenu(accessToken.getAccess_token());
        log.info(json1);
    }

    @Test//查询菜单
    public void queryMenu() throws Exception {
        String url = ACCESS_TOKEN_URL.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
        String json = HttpClientUtil.getInstance().sendHttpsGet(url);
        AccessToken accessToken = JSON.parseObject(json,AccessToken.class);
        System.out.println("票据"+accessToken.getAccess_token());
        System.out.println("有效时间"+accessToken.getExpires_in());
        String json1 = WeixinUtil.queryMenu(accessToken.getAccess_token());
        log.info(json1);
    }


    /**
     *创建菜单
     * @throws Exception
     */
    @Test
    public void creatMenu() throws Exception {
        String url = ACCESS_TOKEN_URL.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
        String json =HttpClientUtil.getInstance().sendHttpsGet(url);
        AccessToken accessToken = JSON.parseObject(json,AccessToken.class);
        System.out.println("票据"+accessToken.getAccess_token());
        System.out.println("有效时间"+accessToken.getExpires_in());
        int a = WeixinUtil.creatMenu(accessToken.getAccess_token(),JSON .toJSONString(initMenu()));
        System.out.println(a);
    }

    private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

    @Test
    public void getAccessToken() throws Exception {
        String url = ACCESS_TOKEN_URL.replace("APPID",APPID).replace("APPSECRET",APPSECRET);
        String json =HttpClientUtil.getInstance().sendHttpsGet(url);
        AccessToken accessToken = JSON.parseObject(json,AccessToken.class);
        log.info(JSON.toJSON(accessToken).toString());
    }


}